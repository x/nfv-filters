============
Installation
============

At the command line::

    $ pip install nfv-filters

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv nfv-filters
    $ pip install nfv-filters
