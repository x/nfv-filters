===============================
nfv-filters
===============================

Nova scheduler filters useful in NFV deployments

Please fill here a long description which must be at least 3 lines wrapped on
80 cols, so that distribution package maintainers can use it in their packages.
Note that this is a hard requirement.

* Free software: Apache license
* Documentation: http://docs.openstack.org/developer/nfv-filters
* Source: http://git.openstack.org/cgit/openstack/nfv-filters
* Bugs: http://bugs.launchpad.net/nfv-filters

Features
--------

* TODO
